<?php
ini_set("soap.wsdl_cache_enabled", "0");
header('Content-Type: application/json');

try {
    $sClient = new SoapClient('http://api.chartlyrics.com/apiv1.asmx?WSDL');

    // Get the necessary parameters from the request
    // Use $sClient to call the operation SearchLyricText
    // echo the returned info as a JSON array of objects

//  header(':', true, 501); // Just remove this line to return the successful
//                          // HTTP-response status code 200.
//    echo $sClient;
    $params = new stdClass();
    $params->lyricText = $_GET['text'];
    $response = $sClient->SearchLyricText($params);


    echo json_encode($response->SearchLyricTextResult->SearchLyricResult);


} catch (SoapFault $e) {
    header(':', true, 500);
    echo json_encode($e);
}

function compare_some_objects($a, $b)
{ // Make sure to give this a more meaningful name!
    return $a->Song - $b->Song;
}

function console_log($data)
{
    echo '<script>';
    echo 'console.log(' . json_encode($data) . ')';
    echo '</script>';
}

?>
