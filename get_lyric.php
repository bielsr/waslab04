<?php
ini_set("soap.wsdl_cache_enabled", "0");
header('Content-Type: application/json');

try {

    $sClient = new SoapClient('http://api.chartlyrics.com/apiv1.asmx?WSDL');

    // Get the necessary parameters from the request
    // Use $sClient to call the operation GetLyric
    // echo the returned info as a JSON object

    $params = new stdClass();
    $params->lyricId = $_GET['id'];
    $params->lyricCheckSum = $_GET['checksum'];
    $response = $sClient->GetLyric($params);
    echo json_encode($response->GetLyricResult);

} catch (SoapFault $e) {
    header(':', true, 500);
    echo json_encode($e);
}

