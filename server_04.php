<?php

ini_set("soap.wsdl_cache_enabled", "0");
$server = new SoapServer("http://localhost:8080/waslab04/WSLabService.wsdl");

function FahrenheitToCelsius($fdegree)
{
    $cresult = ($fdegree - 32) * (5 / 9);
    return array("cresult" => $cresult, "timeStamp" => date('c', time()));
}

function CurrencyConverter($from_Currency, $to_Currency, $amount)
{
    $uri = "http://currencies.apps.grandtrunk.net/getlatest/$from_Currency/$to_Currency";
    $rate = doubleval(file_get_contents($uri));
    return round($amount * $rate, 2);
}

;

function CurrencyConverterPlus($request)
{
    $res = array();
    $to_Currencies = $request->to_Currencies;
    $from_Currency = $request->from_Currency;
    $amount = $request->amount;
    for ($i = 0; $i < count($to_Currencies); ++$i) {
        $to_Currency = $to_Currencies[$i];
        $uri = "http://currencies.apps.grandtrunk.net/getlatest/$from_Currency/$to_Currency";
        $rate = doubleval(file_get_contents($uri));
        $res[$i] = new stdClass();
        $res[$i]->currency = $to_Currency;
        $res[$i]->amount = round($amount * $rate, 2);
    }
    return $res;

}

;

// Task #4: Implement here the CurrencyConverterPlus function and add it to $server

$server->addFunction("CurrencyConverterPlus");
$server->addFunction("FahrenheitToCelsius");

// Task #3 -> Uncomment the following line:
$server->addFunction("CurrencyConverter");

$server->handle();

?>
