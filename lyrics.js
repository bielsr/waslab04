function showSongs() {
    var content = document.getElementById("content").value;
    // Replace the two lines below with your implementation

    let container = document.getElementById("left");
    container.innerHTML = '';
    getLyric(content, container)
};


function getLyric(text, container) {
    const xhr = new XMLHttpRequest()
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            let res = JSON.parse(xhr.responseText);
            res.filter(s => s != null)
                .map(s => {
                    const div = document.createElement('div');
                    div.innerHTML = `<div>
                        <a href="${s.SongUrl}">${s.Song}</a>
                        <span> (${s.Artist})</span>
                        <button onclick='fetchLyricDetails(${s.LyricId}, "${s.LyricChecksum}")'>Lyrics</button>
                    </div>`
                    return div;
                }).forEach(e => container.appendChild(e))
        }
    }
    xhr.open('GET', `http://localhost:8080/waslab04/lyric_search.php?text=${text}`)
    xhr.send()

}

function fetchLyricDetails(id, checksum) {
    const xhr = new XMLHttpRequest()
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            let res = JSON.parse(xhr.responseText);
            let container = document.getElementById("right");
            container.innerHTML = `
                <pre>${res.Lyric}</pre>`;
        }
    }
    xhr.open('GET', `http://localhost:8080/waslab04/get_lyric.php?id=${id}&checksum=${checksum}`)
    xhr.send()

}

window.onload = showSongs();
